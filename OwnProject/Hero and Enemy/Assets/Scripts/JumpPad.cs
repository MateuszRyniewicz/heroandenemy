﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpPad : MonoBehaviour
{
    public SpriteRenderer sr;

    public Sprite downSprite, upSprite;

    public float stayUpTime, bouncerPower;
    private float upCounter;

    // Start is called before the first frame update
    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (upCounter > 0)
        {
            upCounter -= Time.deltaTime;

            if (upCounter < 0)
            {
                sr.sprite = downSprite;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            upCounter = stayUpTime;
            sr.sprite = upSprite;

            Rigidbody2D rbPlayer = other.GetComponent<Rigidbody2D>();
            rbPlayer.velocity = new Vector2(rbPlayer.velocity.x, bouncerPower);
           
            
        }
    }
}
