﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed = 7.5f;
    public Rigidbody2D rb;
    public GameObject bulletImpactEffect;

    public int damageOfValue;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();

        
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity  = transform.right * speed;

    }

    private void OnTriggerEnter2D(Collider2D other)
    {

        if (other.tag == "Enemy")
        {

            other.GetComponent<EnemyController>().DamageEnemy(damageOfValue);
        } 

        Instantiate(bulletImpactEffect, transform.position, transform.rotation);
        Destroy(gameObject);

        AudioManager.instance.PlaySfx(4);


    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
