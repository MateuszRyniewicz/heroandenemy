﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject target;
    private Vector3 curretVelocity;
    public float timeSmoth;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 newCameraPos = new Vector3(target.transform.position.x, transform.position.y, transform.position.z);

        transform.position = Vector3.SmoothDamp(transform.position,newCameraPos, ref curretVelocity,timeSmoth);
    }
}
