﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public Transform[] points;
    public float speed;
    public int curretPoint;

    public Transform platform;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        platform.position = Vector3.MoveTowards(platform.position, points[curretPoint].position, speed * Time.deltaTime);

        if (Vector3.Distance(platform.position, points[curretPoint].position) < 0.5f)
        {
            curretPoint++;

            if(curretPoint >= points.Length)
            {
                curretPoint = 0;
            }
        }
    }
}
