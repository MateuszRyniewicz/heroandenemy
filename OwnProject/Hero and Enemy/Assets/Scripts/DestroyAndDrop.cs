﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAndDrop : MonoBehaviour
{

    public bool shouldDrop;
    public GameObject[] itemsToDrop;
    public float itemToDropProcent;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "PlayerBullet")
        {
            Destroy(gameObject);

            AudioManager.instance.PlaySfx(0);

            if (shouldDrop)
            {
                float chance = Random.Range(0f, 100f);

                if (chance < itemToDropProcent)
                {
                    int randomItem = Random.Range(0, itemsToDrop.Length);
                    Instantiate(itemsToDrop[randomItem], transform.position, transform.rotation);
                }
            }
        }
    }
}
