﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    [HideInInspector]
    public Rigidbody2D rb;
    public float moveSpeed;

    [Header("Chase Player")]
    public bool shouldChasePlayer;
    public float rangeToChasePlayer;
    private Vector3 runDirection;

    [Header("Run away")]
    public bool shouldRunAway;
    public float runWayRange;

    [Header("Patrol")]
    public bool shouldPatrol;
    public Transform[] patrolPoints;
    private int currentPatrolPoint;

    public Animator anim;

    public int health =3;
    [Header("Effect")]
    public GameObject []splatters;
    public GameObject enemyHitEffect;

    [Header("Shoot Effect")]
    public bool shouldShoot;
    public GameObject bullet;
    public Transform firePoint;
    public float fireRate;
    public float fireCounter;
    public float shootDistance;

    public SpriteRenderer sr;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        
       
    }

    // Update is called once per frame
    void Update()
    {
        if(sr.isVisible && PlayerController.instace.gameObject.activeInHierarchy)
        {

            runDirection = Vector3.zero;

            if (Vector3.Distance(transform.position, PlayerController.instace.transform.position) < rangeToChasePlayer && shouldChasePlayer)
            {
                runDirection = PlayerController.instace.transform.position - transform.position;

                if (runDirection.y > 0.1)
                {
                    runDirection = new Vector3(runDirection.x, 0f, 0f);
                }

            }

            if(shouldRunAway && Vector3.Distance(transform.position, PlayerController.instace.transform.position) < runWayRange)
            {

                runDirection = new Vector3(transform.position.x - PlayerController.instace.transform.position.x,runDirection.y);

                if (runDirection.y > 0.1f)
                {
                    runDirection = new Vector3(runDirection.x, 0f, 0f);
                }
                else if(runDirection.x <=0)
                {
                    runDirection = new Vector3(0f,runDirection.y,0f);
                }
            }

            if (shouldPatrol)
            {
                runDirection = patrolPoints[currentPatrolPoint].position - transform.position;

                if (Vector3.Distance(transform.position, patrolPoints[currentPatrolPoint].position) < 0.2f)
                {
                    currentPatrolPoint++;
                    if (currentPatrolPoint >= patrolPoints.Length)
                    {
                        currentPatrolPoint = 0;
                    }
                }
            }


            runDirection.Normalize();
            rb.velocity = runDirection * moveSpeed;


           



            if (shouldShoot && Vector3.Distance(transform.position, PlayerController.instace.transform.position) < shootDistance)
            {


                {
                    fireCounter -= Time.deltaTime;

                    if (fireCounter <= 0)
                    {
                        fireCounter = fireRate;

                        Instantiate(bullet, firePoint.transform.position, firePoint.transform.rotation);
                        AudioManager.instance.PlaySfx(13);
                    }
                }
            }

        }
        else
        {
            rb.velocity = Vector2.zero;
        }

        AnimationEnemyIdleAndRun();

    }

    public void DamageEnemy(int damage)
    {
        health -= damage;

        AudioManager.instance.PlaySfx(2);

        Instantiate(enemyHitEffect, transform.position, transform.rotation);


        if (health <= 0)
        {
            
            Destroy(gameObject);

            AudioManager.instance.PlaySfx(1);

            int selectSpatter = Random.Range(0, splatters.Length);

            int rotation = Random.Range(0, 4);

            Instantiate(splatters[selectSpatter], transform.position, Quaternion.Euler(0f ,0f ,rotation * 90));
        }
    }

    public void AnimationEnemyIdleAndRun()
    {
        if (runDirection != Vector3.zero)
        {
            anim.SetBool("isMoving", true);
        }
        else
        {
            anim.SetBool("isMoving", false);
        }
    }
}
