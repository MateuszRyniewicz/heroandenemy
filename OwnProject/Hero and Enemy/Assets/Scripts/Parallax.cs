﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    public Transform cameraTransform;
    public float paralaxFactor;

    private Vector3 prevCameraPos;
    private Vector3 delataCameaPso;

    // Start is called before the first frame update
    void Start()
    {
        prevCameraPos = cameraTransform.position;   
    }

    // Update is called once per frame
    void Update()
    {
        delataCameaPso = cameraTransform.position - prevCameraPos;
        Vector3 paralaxPso = new Vector3(transform.position.x + (delataCameaPso.x * paralaxFactor), transform.position.y, transform.position.z);
        transform.position = paralaxPso;
        prevCameraPos = cameraTransform.position;
    }
}
