﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickup : MonoBehaviour
{
    public int healthOfValue = 1;

    public bool isHealth;
    public bool isInvincible;

    [Header("Coin")]
    public bool isCoin;
    private bool isCollected;
   

    public float powerUpLenght;

    [Header("Effect")]
    public GameObject healthPickupEffect;
    public GameObject InvicibilityPickupEffect;

    public float waitAfterBeCollected = 0.5f;


    // Update is called once per frame
    void Update()
    {
        if (waitAfterBeCollected > 0)
        {
            waitAfterBeCollected =- Time.deltaTime;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player" && waitAfterBeCollected <= 0)
        {

            if (isHealth)
            { 
                PlayerHealthController.instace.HealthPlayerAdd(healthOfValue);
                Instantiate(healthPickupEffect, transform.position, transform.rotation);

                
            }

            if (isInvincible)
            {
                PlayerHealthController.instace.MakeInvincible(powerUpLenght);
                Instantiate(InvicibilityPickupEffect, transform.position, transform.rotation);

                
            }

            if (isCoin && !isCollected)
            {
                PlayerHealthController.instace.coins++;

                isCollected = true;

                UIController.instace.UpdateCoin();
                
            }

            AudioManager.instance.PlaySfx(7);
            Destroy(gameObject);
        }
    }
}
