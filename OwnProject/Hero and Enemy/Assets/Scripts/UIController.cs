﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;

public class UIController : MonoBehaviour
{
    public static UIController instace;


    public Text coinText;

    public Slider healthSlider;
    public Text healthText;

    [Header("Fade System")]
    public Image fadeScreen;
    public float fadeSpeed;
    private bool fadeTOBlack;
    private bool fadeOUTBlack;
    

    public GameObject gameOverScreen;
    public GameObject pauseScreen;

    public string MainMenuScreen;
    public string level;

    private void Awake()
    {
        instace = this;
    }

    private void Start()
    {
        UpdateCoin();

        fadeOUTBlack = true;
        fadeTOBlack = false;
    }

    private void Update()
    {
        if (Keyboard.current.escapeKey.wasPressedThisFrame)
        {
            PauseUnPause();
        }

        if (fadeOUTBlack)
        {
            fadeScreen.color = new Color(fadeScreen.color.r, fadeScreen.color.g, fadeScreen.color.b, Mathf.MoveTowards(fadeScreen.color.a, 0f, fadeSpeed * Time.deltaTime));
            if (fadeScreen.color.a == 0f)
            {
                fadeOUTBlack = false;
            }
        }

        if (fadeTOBlack)
        {
            fadeScreen.color = new Color(fadeScreen.color.r, fadeScreen.color.g, fadeScreen.color.b, Mathf.MoveTowards(fadeScreen.color.a, 1f, fadeSpeed * Time.deltaTime));
            if (fadeScreen.color.a == 1f)
            {
                fadeTOBlack = false;
            }
        }
    }

    public void StartFadeToBlack()
    {
        fadeTOBlack = true;
        fadeOUTBlack = false;

        //i have to add this method to level end when player goes to the sphere...
    }

    public void UpdateCoin()
    {
        coinText.text = PlayerHealthController.instace.coins.ToString();
    }

    public void PauseUnPause()
    {
        if (pauseScreen.activeInHierarchy)
        {
            pauseScreen.SetActive(false);

            Time.timeScale = 1f;
        }
        else
        {
            pauseScreen.SetActive(true);

            Time.timeScale = 0f;
        }
    }

    public void MainMenu()
    {
        SceneManager.LoadScene(MainMenuScreen);

        Time.timeScale = 1f;
    }
    
    public void NewGame()
    {
        SceneManager.LoadScene(level);

        Time.timeScale = 1f;
    }


    public void QuitGame()
    {
        Application.Quit();

#if UNITY_EDITOR

        UnityEditor.EditorApplication.isPlaying = false;
#endif
    }
}
