﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    public static PlayerController instace;


    [HideInInspector]
    public Rigidbody2D rb;

    [HideInInspector]
    public Animator anim;

    [HideInInspector]
    public SpriteRenderer sr;

    [HideInInspector]
    public float powerUpCounter;

    [HideInInspector]
    public bool canMove = true;

    public float speed;
    public float jumpForce;

    private float velocity;

    [HideInInspector]
    public bool isGrounded;
    public Transform groundCheckPoint;
    public LayerMask whatIsGround;

    [Header("Shoot Attack")]
    public float timeBetweenAttack;
    private float attackCounter;
    public GameObject playerBullet;
    public Transform firePoit;
    public float timeBetweenShots;
    private float shotCounter;

    public bool isFlip;
    private float activeMoveSpeed;

    [Header("Dash")]
    public float dashSpeed=50f;
    public float dashLenght=0.5f;
    public float dashCoolDown = 1f;
    public float dashInvibility = 0.5f;
    private float dashCounter;
    private float dashCoolCounter;
    public GameObject dashEffect;
    public Transform dashPoint;

    



    public void Awake()
    {
     
            instace = this;
            
       
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();
        
    }

    private void Start()
    {
        activeMoveSpeed = speed;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.timeScale != 0 & canMove)
        {


            isGrounded = Physics2D.OverlapCircle(groundCheckPoint.position, 0.2f, whatIsGround);


            rb.velocity = new Vector2(velocity * activeMoveSpeed, rb.velocity.y);


            anim.SetBool("isGrounded", isGrounded);
            anim.SetFloat("ySpeed", rb.velocity.y);
            anim.SetFloat("speed", Mathf.Abs(rb.velocity.x));

            if (rb.velocity.x < 0)
            {
                transform.localScale = new Vector3(-1, 1, 1);
                firePoit.transform.localScale = new Vector3(-1, 1, 1);
                dashPoint.transform.localScale = new Vector3(-1, 1, 1);

                isFlip = true;
            }
            else if (rb.velocity.x > 0)
            {
                transform.localScale = Vector2.one;
                firePoit.transform.localScale = Vector2.one;
                dashPoint.transform.localScale = Vector2.one;
                isFlip = false;

            }


            if (attackCounter > 0f)
            {
                attackCounter -= Time.deltaTime;

                rb.velocity = new Vector2(0f, rb.velocity.y);
                // I have to do add some effect;
            }


            if (Keyboard.current.leftAltKey.wasPressedThisFrame)
            {
                FireBullet();
            }



            if (Keyboard.current.leftCtrlKey.wasPressedThisFrame)
            {

                Debug.Log("key left crtl;");
                if (dashCoolCounter <= 0 && dashCounter <= 0)
                {
                    activeMoveSpeed = dashSpeed;
                    dashCounter = dashLenght;
                    Instantiate(dashEffect, dashPoint.transform.position, dashPoint.transform.rotation);

                    AudioManager.instance.PlaySfx(8);

                }
            }

            if (dashCounter > 0)
            {
                dashCounter -= Time.deltaTime;
                if (dashCounter <= 0)
                {
                    activeMoveSpeed = speed;
                    dashCoolCounter = dashCoolDown;
                }
            }

            if (dashCoolCounter > 0)
            {
                dashCoolCounter -= Time.deltaTime;
            }

        }
    }

    public void Move(InputAction.CallbackContext context)
    {
        velocity = context.ReadValue<Vector2>().x;
    }

    public void Jump(InputAction.CallbackContext context)
    {

        if (context.started && isGrounded)
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpForce);
            AudioManager.instance.PlaySfx(6);
        }

        if(!isGrounded && context.canceled && rb.velocity.y > 0f)
        {
            rb.velocity = new Vector2(rb.velocity.y, rb.velocity.y * 0.5f);
        }
    }

    public void Attack( InputAction.CallbackContext context)  
    {
        if (context.started)
        {
            anim.SetTrigger("attack");

            attackCounter = timeBetweenAttack;
        }
    }

    public void FireBullet()
    {
        if (!isFlip)
        {
            shotCounter -= Time.deltaTime;

            if (shotCounter <= 0)
            {
                Instantiate(playerBullet, firePoit.position, Quaternion.identity);
                shotCounter = timeBetweenShots;

                AudioManager.instance.PlaySfx(12);
                    
            }

        }
        else
        {
            shotCounter -= Time.deltaTime;

            if (shotCounter <= 0)
            {
                Instantiate(playerBullet, firePoit.position, Quaternion.Euler(0, 0, 180));

                AudioManager.instance.PlaySfx(12);
                shotCounter = timeBetweenShots;
            }
            
        }
        
       
    }


    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Platform")
        {
            transform.parent = other.transform;
        }
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.tag == "Platform")
        {
            transform.parent = null;
        }
    }

}
