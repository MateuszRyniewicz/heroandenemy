﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwoothDamage : MonoBehaviour
{

    public int swoothDamageOfValue = 1;
    public GameObject swoothEffect;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {

            if (other.tag == "Enemy")
            {

                other.GetComponent<EnemyController>().DamageEnemy(swoothDamageOfValue);
            }

            Instantiate(swoothEffect, transform.position, transform.rotation);
            
        
    }
}
