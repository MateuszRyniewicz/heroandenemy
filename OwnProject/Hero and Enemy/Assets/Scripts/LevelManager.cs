﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public static LevelManager instance;

    public GameObject endFlag;

    public float waitToLoad = 5f;
    public string nextLevel;

    private void Awake()
    {
        instance = this;

        
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public IEnumerator LevelEnd()
    {
        AudioManager.instance.PlayLevelWin();
        PlayerController.instace.canMove = false;

        endFlag.SetActive(true);
        UIController.instace.StartFadeToBlack();

        yield return new WaitForSeconds(waitToLoad);

        GetComponent<SavingPoints>().SavePoints();
        
        SceneManager.LoadScene(nextLevel);
    }
}
