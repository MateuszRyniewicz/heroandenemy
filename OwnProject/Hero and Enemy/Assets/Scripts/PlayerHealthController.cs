﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerHealthController : MonoBehaviour
{
    public static PlayerHealthController instace;

    public int maxHealth = 3;
    public int currentHealth;

    /*
    [Header("Dislapy Heart")]
    public SpriteRenderer []playerHearts;
    public Sprite heartFull;
    public Sprite heartEmpty;
    public Transform heartHolder;

    */
    public float invincbillityTime;
    private float invCounter;

   // [Header("Flash Heart")]
   // public float flashHeart=0.2f;
   // private float flashHeartCounter;

    public int coins;

    private void Awake()
    {

        instace = this;
        coins = GetComponent<SavingPoints>().LoadPoits();

    }

    // Start is called before the first frame update
    void Start()
    {
        UIController.instace.UpdateCoin();

        currentHealth = maxHealth;

        UpdateHealthSlider();
      //  UpdateHealth();
        
    }

    // Update is called once per frame
    void Update()
    {
        
        if (invCounter > 0)
        {
            invCounter -= Time.deltaTime;

            if (invCounter <= 0)
            {
                PlayerController.instace.sr.color = new Color(PlayerController.instace.sr.color.r, PlayerController.instace.sr.color.g, PlayerController.instace.sr.color.b, 1f);
            }
            // go to the DamagePlayer Method's

            /*
            flashHeartCounter -= Time.deltaTime;
            if (flashHeartCounter <= 0)
            {
                flashHeartCounter = flashHeart;

                heartHolder.gameObject.SetActive(!heartHolder.gameObject.activeInHierarchy);
            }

            if (invCounter <= 0)
            {
                heartHolder.gameObject.SetActive(true);
            }
            */
           
        }

        

    }

   // private void LateUpdate()
   // {
   //     heartHolder.localScale = transform.localScale;
        
   // }

   /* public void UpdateHealth()
    {
        switch (currentHealth)
        {
            case 3:
                playerHearts[0].sprite = heartFull;
                playerHearts[1].sprite = heartFull;
                playerHearts[2].sprite = heartFull;
                break;

            case 2:
                playerHearts[0].sprite = heartFull;
                playerHearts[1].sprite = heartFull;
                playerHearts[2].sprite = heartEmpty; 
                break;
            case 1:
                playerHearts[0].sprite = heartFull;
                playerHearts[1].sprite = heartEmpty;
                playerHearts[2].sprite = heartEmpty;
                break;

            case 0:
                playerHearts[0].sprite = heartEmpty;
                playerHearts[1].sprite = heartEmpty;
                playerHearts[2].sprite = heartEmpty;
                break;
        }
    }

    */

    public void DamagePlayer(int dmgAmount)
    {

        if (invCounter <= 0)
        {


            currentHealth -= dmgAmount;
            AudioManager.instance.PlaySfx(11);


            if (currentHealth < 0)
            {
                currentHealth = 0;
                
            }

           // UpdateHealth();
            UpdateHealthSlider();

            if (currentHealth == 0)
            {
                DeathEffect();
            }


            invCounter = invincbillityTime;

            PlayerController.instace.sr.color = new Color(PlayerController.instace.sr.color.r, PlayerController.instace.sr.color.g, PlayerController.instace.sr.color.b, 0.5f);

        }
    }

    public void HealthPlayerAdd(int healthToAdd)
    {
        currentHealth += healthToAdd;

        if (currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }

      // UpdateHealth();
        UpdateHealthSlider();

      //  flashHeartCounter = 0;
        invCounter = 0;

        
    }

    public void MakeInvincible(float invinceLenght)
    {
        invCounter = invinceLenght;

    }

    public void DeathEffect()
    {
        PlayerController.instace.gameObject.SetActive(false);

        UIController.instace.gameOverScreen.SetActive(true);

        AudioManager.instance.PlayGameOver();

        AudioManager.instance.PlaySfx(8);
    }



    public void UpdateHealthSlider()
    {
        
        UIController.instace.healthSlider.maxValue = maxHealth;
        UIController.instace.healthSlider.value = currentHealth;
        UIController.instace.healthText.text = currentHealth + " / " + maxHealth;
    }

}
