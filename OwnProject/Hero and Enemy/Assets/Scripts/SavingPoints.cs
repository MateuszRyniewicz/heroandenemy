﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SavingPoints : MonoBehaviour
{
   public void SavePoints()
    {
        PlayerPrefs.SetInt("points", PlayerHealthController.instace.coins);
        PlayerPrefs.Save();
    }

    public int LoadPoits()
    {
        if (PlayerPrefs.HasKey("points"))
        {
            return PlayerPrefs.GetInt("points");
        }
        else
        {
            return 0;
        }
    }
}
